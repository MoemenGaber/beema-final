$(document).ready(function() {

	"use strict";

	/* ================= sliders =================  */
			/* =================  Owl Carousel =============  */

			$(".services_slider").owlCarousel({
				loop: true,
				nav: true,
				dots: false,
				autoplay: true,
		 	    autoplayTimeout: 4000,
				autoplayHoverPause:false,
		 	    smartSpeed:1000,
				center:true,
				margin : 0,
				items : 1
			});
			$(".hero-items-slider").owlCarousel({
				loop: true,
				nav: false,
				dots: true,
				autoplay: true,
		 	    autoplayTimeout: 4000,
				autoplayHoverPause:false,
		 	    smartSpeed:1000,
				center:false,
				margin : 20,
				items : 2
			});

			/* =================  Owl Carousel =============  */

			$(".blogs_slider").owlCarousel({
				loop: true,
				nav: true,
				dots: false,
				autoplay: true,
		 	    autoplayTimeout: 4000,
				autoplayHoverPause:false,
		 	    smartSpeed:1000,
				margin : 30,
				responsiveClass: true,
				responsive: {
					0:{
						items:1,
					},
					575:{
						items:2,
					},
					991:{
						items:3,
					}
				}
			});
		
			/* =================  Owl Carousel =============  */

			$(".logos").owlCarousel({
				loop: true,
				nav: true,
				dots: false,
				autoplay: true,
		 	    autoplayTimeout: 4000,
				autoplayHoverPause:false,
		 	    smartSpeed:1000,
				margin : 50,
				responsiveClass: true,
				responsive: {
					0:{
						items:3,
					},
					575:{
						items:4,
					},
					991:{
						items:5,
					}
				}
			});
			// ============= rev slider =====================
			$("#rev_slider_1042_1").show().revolution();
});

/* =================  window load =================  */

 $(window).on('load',function(){
		/*----- loader ---------*/
		$('.loader').fadeOut();
		/* ==== accordion ===== */
		var  acc_numbers= 0;
		var  acc_numbers_child= 0;
		$('.quarterlyResults .container').children('.accordion').each(function(){
			$(this).attr('id',"acc_" + acc_numbers);
			$(this).find('.card-header').attr('id',"h_" + acc_numbers);
			$(this).find('.collapse').attr('id',"b_" + acc_numbers).attr("aria-labelledby","h_" + acc_numbers).attr("data-parent","acc_" + acc_numbers);
			$(this).find("button").attr("data-target","#b_" + acc_numbers).attr("aria-controls","b_" + acc_numbers);
			
			$(this).find('.accordion').each(function(){
				$(this).attr('id',"acc_child_" + acc_numbers_child);
				$(this).find('.card-header').attr('id',"h_child_" + acc_numbers_child);
				$(this).find('.collapse').attr('id',"b_child_" + acc_numbers_child).attr("aria-labelledby","h_child_" + acc_numbers_child).attr("data-parent","acc_child_" + acc_numbers_child);
				$(this).find("button").attr("data-target","#b_child_" + acc_numbers_child).attr("aria-controls","b_child_" + acc_numbers_child);
				acc_numbers_child +=1 ;
			})
			acc_numbers +=1 ;
		});

    /*----- WoW Animations ---------*/
      wow = new WOW();
    	wow.init();
});

/* =================  window Scroll =================  */

 $(window).on('scroll , load',function(){
		var window_top = $(window).scrollTop();
		/*---------- menu fixed ----------*/

		if(window_top > 20){
			$('.site_header').addClass('header-scroll_bg_light');
		}
		else {
			$('.site_header').removeClass('header-scroll_bg_light');
		}
	

/*---------- go to top button ---------*/
    if(window_top > 600){
      $('.goto_top').fadeIn();
    }
    else {
      $('.goto_top').fadeOut();
    }
	});

  $('.goto_top').on('click',function(e){
    e.preventDefault();
		$('body , html').animate({
			scrollTop: 0
		},1000);
  })
/* =================  menu click animate =================  */

	$('.nav-item .nav-link , .scrollDown').on('click',function(){
		var $target = $(this).attr('href');
		$('body , html').animate({
			scrollTop: $($target).position().top
		},1000);
		$('.navbar-collapse').removeClass('show');
		$('.navbar-toggler svg').toggleClass('fa-times').toggleClass('fa-bars');
	});
	$('.navbar-toggler').on('click',function(){
		$('.navbar-toggler svg').toggleClass('fa-times').toggleClass('fa-bars');
	});
/* =================  card click =================  */
$('.card-header button').on('click',function(){
	var card = $(this).parents('#accordion').find('.card');
	card.removeClass('open');
	card.find('.card-header button').removeClass('active');
	card.find('.collapse').removeClass('show');
	$(this).addClass('active');
	$(this).parents('.card').addClass('open');
});
/* ====== taps ======= */
$('.card-header').on('click',function(){
	var tab = $(this).attr('target');
	$('.tab').removeClass('show');
	$(tab).addClass('show');
})

/* contact us tabs */
$('.maintaps li ._main').on("click" , function(e){
	e.preventDefault();
	var id=$(this).attr("href");
	$('.maintaps li ._main').removeClass("active");
	$(this).addClass("active");
	$('.maintaps .tap').removeClass("active");
	$(id).addClass("active");
  })

//======== taps ==============
$('.tabs_sec a').on('click',function(){
	var tab_name = $(this).attr('target');
	$('.tab-pane').removeClass('show active');
	$(tab_name).addClass('show active');
})
// --------- menu click ---------
$('.menu .nav-item .nav-link').on('click',function(){
	var $target = $(this).attr('href') ;
	var $top = $target.offset().top ;
	$('body , html').animate({
		scrollTop: $top
	},1000);
	$('.nav-item').removeClass('active');
	$(this).parent().addClass('active');
});