<?php 
get_header(); 
?>

      <main class="site_main">
        <section class="topSection" style="background: url(..<?php echo get_template_directory_uri();?>/img/retail.jpg);">
          <div class="container">
            <h1><?php the_title(); ?></h1>
            <p><?php the_field('service_excerpt'); ?></p>
          </div>
        </section>
        <section class="serviceOverview">
          <div class="container">
            <h2 class="textDarkBlue"><?php $servies_overview=get_field('service_overview_title'); if($servies_overview){the_field('service_overview_title');}else{echo "Service Overview";} ?></h2>
            <p><?php the_field('service_overview_description'); ?></p>
            <div class="margin_top_4">
              <div class="row">
                <div class="col-lg-6"><img class="w-100" src="<?php the_field('first_image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold"><?php the_field('first_paragraph_title'); ?></h3>
                  <p>
                   <?php the_field('first_paragraph'); ?>
                  </p><a class="green_btn btn_style" href="<?php the_field('first_paragraph_request_a_call_link'); ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold"><?php the_field('second_paragraph_title'); ?></h3>
                  <p>
                    <?php the_field('second_paragraph'); ?>
                  </p><a class="green_btn btn_style" href="<?php the_field('first_paragraph_request_a_call_link'); ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php the_field('second_image'); ?>" alt="pic"/></div>
              </div>
             
            </div>
          </div>
        </section>
      </main>
    </body>

<?php get_footer();