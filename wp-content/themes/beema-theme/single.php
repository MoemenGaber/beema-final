<?php

/* Template Name: Claims */

get_header(); ?>

    <main class="site_main">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/group1.jpg);">
            <div class="container">
                <h1><?php the_title(); ?></h1>
            </div>
        </section>
        <section class="claims">
            <div class="container">
                <img class="single-news-image" src="<?php echo get_the_post_thumbnail_url(); ?>">
               <?php the_content(); ?>
            </div>
        </section>


    </main>


<?php get_footer();