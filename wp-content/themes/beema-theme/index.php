<?php get_header(); ?>
      <main class="site_main">
          <section class="pt-0 pb-0" id="hero_section">
              <div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_1042_1_wrapper" data-alias="youtube-hero" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                  <div class="rev_slider fullwidthabanner d-flex align-items-center" id="rev_slider_1042_1" data-version="5.4.1">
                      <ul>
                          <li data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1000">
                              <!--required for background video, and will serve as the video's "poster/cover" image--><img class="rev-slidebg" src="<?php echo get_template_directory_uri(); ?>/assets/img/youtubebg.jpg" alt="Ocean" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"/>
                              <!-- HTML5 BACKGROUND VIDEO LAYER -->
                              <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" data-ytid="8dTzy_N0Pn4" data-videoattributes="version=3&amp;amp;enablejsapi=1&amp;amp;html5=1&amp;amp;hd=1&amp;amp;wmode=opaque&amp;amp;showinfo=0&amp;amp;rel=0;" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videostartat="00:06" data-videoendat="02:00" data-videoloop="loop" data-forceCover="0" data-aspectratio="4:3" data-autoplay="true" data-autoplayonlyfirsttime="true"></div>
                          </li>
                      </ul>
                  </div>
              </div>
              <div class="container">
                  <div class="row">
                      <div class="col-lg-6">
                          <h1><?php the_field('main_slider_title',250); ?></h1>
                          <p><?php the_field('slider_description',250); ?></p>
                         <a href="tel:<?php the_field('phone_number',250); ?>"><h1 id="phone-number-slider"> <i class="fas fa-phone-volume"></i> <?php the_field('phone_number',250); ?></h1></a>
                      </div>
                      <div class="col-lg-6">
                <div class="tabs_sec">
                  <ul class="nav nav-tabs justify-content-between" id="myTab" role="tablist">
                    <li><a class="active" data-toggle="tab" href="#quote" role="tab" aria-controls="quote" aria-selected="true">Quote</a></li>
                    <li><a data-toggle="tab" href="#renew" role="tab" aria-controls="renew" aria-selected="true">RENEW</a></li>
                    <li><a data-toggle="tab" href="#claims" role="tab" aria-controls="claims" aria-selected="true">Claims</a></li>
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="quote" role="tabpanel" aria-labelledby="quote-tab">
                      <p>Our options in Retail insurance</p>
                      <div class="pc-view">   
                        <div class="row flex-nowrap">
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/motor-icon.png" alt="alt"/><small>Motor Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/home-icom.png" alt="alt"/><small>Home Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/travel-icon.png" alt="alt"/><small>Travel Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/boat-icon.png" alt="alt"/><small>Boat Insurance</small></a></div>
                        </div>
                        <div class="row flex-nowrap">
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/life-icon.png" alt="alt"/><small>Life Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/maid-icon.png" alt="alt"/><small>Maid Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon2.png" alt="alt"/><small>Insurance</small></a></div>
                        </div>
                      </div>
                      <div class="owl-carousel hero-items-slider"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/motor-icon.png" alt="alt"/><small>Motor Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/home-icom.png" alt="alt"/><small>Home Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/travel-icon.png" alt="alt"/><small>Travel Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/boat-icon.png" alt="alt"/><small>Boat Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/life-icon.png" alt="alt"/><small>Life Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/maid-icon.png" alt="alt"/><small>Maid Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a></div>
                    </div>
                    <div class="tab-pane fade" id="renew" role="tabpanel" aria-labelledby="renew-tab">
                      <p>Our options in Retail insurance</p>
                      <div class="pc-view">   
                        <div class="row flex-nowrap">
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/motor-icon.png" alt="alt"/><small>Motor Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/home-icom.png" alt="alt"/><small>Home Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/travel-icon.png" alt="alt"/><small>Travel Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/boat-icon.png" alt="alt"/><small>Boat Insurance</small></a></div>
                        </div>
                        <div class="row flex-nowrap">
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/life-icon.png" alt="alt"/><small>Life Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/maid-icon.png" alt="alt"/><small>Maid Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon2.png" alt="alt"/><small>Insurance</small></a></div>
                        </div>
                      </div>
                      <div class="owl-carousel hero-items-slider"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/motor-icon.png" alt="alt"/><small>Motor Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/home-icom.png" alt="alt"/><small>Home Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/travel-icon.png" alt="alt"/><small>Travel Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/boat-icon.png" alt="alt"/><small>Boat Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/life-icon.png" alt="alt"/><small>Life Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/maid-icon.png" alt="alt"/><small>Maid Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a></div>
                    </div>
                    <div class="tab-pane fade" id="claims" role="tabpanel" aria-labelledby="claims-tab">
                      <p>Our options in Retail insurance</p>
                      <div class="pc-view">   
                        <div class="row flex-nowrap">
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/motor-icon.png" alt="alt"/><small>Motor Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/home-icom.png" alt="alt"/><small>Home Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/travel-icon.png" alt="alt"/><small>Travel Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/boat-icon.png" alt="alt"/><small>Boat Insurance</small></a></div>
                        </div>
                        <div class="row flex-nowrap">
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/life-icon.png" alt="alt"/><small>Life Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/maid-icon.png" alt="alt"/><small>Maid Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a></div>
                          <div class="col-sm-3"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon2.png" alt="alt"/><small>Insurance</small></a></div>
                        </div>
                      </div>
                      <div class="owl-carousel hero-items-slider"><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/motor-icon.png" alt="alt"/><small>Motor Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/home-icom.png" alt="alt"/><small>Home Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/travel-icon.png" alt="alt"/><small>Travel Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/boat-icon.png" alt="alt"/><small>Boat Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/life-icon.png" alt="alt"/><small>Life Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/maid-icon.png" alt="alt"/><small>Maid Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a><a class="tab_item" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/insurance-icon.png" alt="alt"/><small>Insurance</small></a></div>
                    </div>
                  </div>
                </div>
                <div class="row pl-4 pr-4 mt-3">
                  <div class="mr-5 mb-4">
                    <h3 class="mb-0 font-weight-bold">2009 <br></h3><small>Since</small>
                  </div>
                  <div class="mr-5 mb-4">
                    <h3 class="mb-0 font-weight-bold">9% </h3><small>Surplus return</small>
                  </div>
                  <div> 
                    <!-- <p>We served more than 999 clients <br> across qatar</p> -->
                  </div>
                </div>
              </div>
                  </div><a class="scrollDown" href="#services_section"><i class="fas fa-arrow-down mr-2"> </i>Scroll Down </a>
              </div>
          </section>
        <section id="services_section">
          <div class="container">
            <p>We have an amazing service</p>
            <h2>The smoothest Insurance service</h2>
            <div class="row">  
              <div class="col-md-6">
                <!-- <h3 class="textBlue _mid">Corporate Insurance</h3> -->
                <div class="services_slider owl-carousel">
                  
                  <!-- Dynamic slider left section Homepage -->
                  <?php if(have_rows('left_section',250)){
                    while(have_rows('left_section',250)){
                      the_row();
                      while(have_rows('slide_item',250)){
                        the_row();
                        ?>
                    
                  <div class="serve_item">
                    <div class="_img"><img src="<?php the_sub_field('slide_item_image'); ?>" alt="pic"/></div>
                    <h3 class="_mid"><?php the_sub_field('slide_item_title'); ?></h3>
                    <p class="mb-4"><?php the_sub_field('slide_item_description'); ?></p>
                  </div>

                    <?php  } } } ?>

                  <!-- End Dynamic slider left section Homepage -->       
                </div>
              </div>
              <div class="col-md-6 left-static-item">
                <!-- <h3 class="textBlue _mid">Retailer Insurance</h3> -->
                <?php if(have_rows('right_section',250)){
                    while(have_rows('right_section',250)){
                      the_row();
                      ?>
                <div class="serve_item">
                <div class="_img left-static-image"><img src="<?php the_sub_field('slide_item_image'); ?>" alt="pic"/></div>
                <h3 class="_mid"><?php the_sub_field('slide_item_title'); ?></h3>
                <p class="mb-4"><?php the_sub_field('slide_item_description'); ?></p>

              </div>
              <?php 
                    } }
              ?>
              
              </div>
            </div>
          </div>
        </section>
        <section id="installApp">
          <div class="container">
            <div class="row">
              <div class="col-md-6 text-center"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/mokup.png" alt="pic"/></div>
              <div class="col-md-6">
                <h2 class="mt-0"><?php the_field('section_title',250); ?></h2>
                <p><?php the_field('section_description',250); ?></p><a class="btn_style green_btn mr-4 mb-3" href="<?php the_field('visit_website_link',250); ?>">Visit website</a><a class="btn_style blackBorder_btn mb-3" href="<?php the_field('download_application_link',250); ?>">Download Application</a>
                <div>
                <a class="btn_style black_btn d-inline-flex align-items-center" href="<?php the_field('apple_store_link',250); ?>"><i class="fab fa-apple _icon mr-2"></i>
                    <p> <small>Download on the</small><br> APP STORE</p></a>
                    <a class="btn_style black_btn d-inline-flex align-items-center ml-4" href="<?php the_field('google_play_link',250); ?>"><i class="fab fa-google-play _icon mr-2"></i>
                    <p> <small>Get it on</small><br> GOOGLE PLAY</p></a></div>
              </div>
            </div>
          </div>
        </section>
      </main>
    <?php get_footer(); ?>