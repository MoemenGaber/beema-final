<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Beema
 */

?>
<DOCTYPE html="html">
  <html>
    <head>
      <meta charset="utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <meta name="description" content=""/>
      <meta name="author" content=""/>
      <title><?php wp_title('');?> <?php if(!is_front_page()){echo "|";} ?> <?php bloginfo('name'); ?></title>
      <!-- css plugins-->
      <?php wp_head(); ?>
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries-->
      <!--if lt IE 9-->
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">              </script>
    </head>
    <body>
    <header class="site_header fixed-top">
        <div class="container navbar-cont bg-white br">
          <nav class="navbar navbar-expand-lg"><a class="navbar-brand" href="<?php echo get_home_url(); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="pic"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto align-items-center">
                <li class="nav-item active"><a class="nav-link" href="#">Insurance</a><i class="ml-2 fas fa-sort-down"></i>
                  <ul class="submenu animated fadeInUp">
                    <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('retail')); ?>">Retail</a><i class="ml-2 fas fa-sort-down"></i>
                      <ul class="submenu animated fadeInUp">
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('motor-insurance')) ?>">Motor Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('travel-insurance')) ?>">Travel Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('home-insurance')) ?>">Home Insurance </a></li>
                      </ul>
                    </li>
                    <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('corporate-overview')); ?>">Corporate</a><i class="ml-2 fas fa-sort-down"></i>
                      <ul class="submenu animated fadeInUp" style="min-width:250px">
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('fire-insurance')) ?>">Property Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('workmens-insurance')) ?>">Workmen’s Compensation Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('liability-insurance')) ?>">Liability Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('engineering-insurance')) ?>">Engineering Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('miscellaneous-insurance')) ?>">Miscellaneous Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('marine-insurance')) ?>">Marine Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('aviation-insurance')) ?>">Aviation Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('group-life-insurance')) ?>">Group life Insurance</a></li>
                        <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('group-medical-insurance')) ?>">Group Medical Insurance</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li class="nav-item"><a class="nav-link" href="<?php echo get_permalink(get_page_by_path('claims')) ?>">Claim</a></li>
                  <li class="nav-item"><a class="nav-link" href="">About us</a><i class="ml-2 fas fa-sort-down"></i>
                      <ul class="submenu animated fadeInUp">
                          </li>
                          <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('company-overview')) ?>">Company Overview</a>
                          <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('team')); ?>">Team</a>
                      </ul>
                  </li>
                   <li class="nav-item active"><a class="nav-link" href="">Media Center</a><i class="ml-2 fas fa-sort-down"></i>
                      <ul class="submenu animated fadeInUp">
                          </li>
                          <li class="nav-item active"> <a class="nav-link" href="#">Publications</a>
                          <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('news')) ?>">Press Releases</a>
                          <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('gallery')); ?>">Gallery</a>
                      </ul>
                </li>
                  <li class="nav-item active"><a class="nav-link" href="<?php echo get_permalink(get_page_by_path('investors-relations')) ?>">Investors Relations</a><i class="ml-2 fas fa-sort-down"></i>
                  <ul class="submenu animated fadeInUp">
                          <li class="nav-item active"> <a class="nav-link" href="#">Financials</a><i class="ml-2 fas fa-sort-down"></i>
                              <ul class="submenu animated fadeInUp">
                                  <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('quarterly-results')) ?>">Quarterly results</a></li>
                                  <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('annual-results')) ?>">Annual reports</a></li>
                              </ul>
                          </li>
                          <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('credit-rating')); ?>">Credit Rating</a></li>
                          <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('surplus-distribution')) ?>">Surplus Distribution</a></li>
                          <li class="nav-item active"> <a class="nav-link" href="<?php echo get_permalink(get_page_by_path('corporate-governance')) ?>">Corporate Governance</a></li>
                          <!-- <li class="nav-item active"> <a class="nav-link" href="#">Financial calendar</a> -->
                      </ul>
                         
                  <li class="nav-item"><a class="nav-link" href="<?php echo get_permalink(get_page_by_path('find-us')); ?>">Find Us</a></li>

                <!-- <li class="nav-item margin_left"><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_path('request-page')) ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a></li> -->
                <li class="nav-item margin_left"><a class="green_btn btn_style" href="<?php the_permalink(get_page_by_path('request-page')); ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a></li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
