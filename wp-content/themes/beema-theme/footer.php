<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Beema
 */

?>
<footer class="site_footer text-center">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-sm-4 col-md-3">
        <h3 class="mb-4"><a class="margin_bottom logo" href="#"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-footer.png" alt="pic"/></a></h3>
      </div>
      <div class="col-sm-4 col-md-3">
        <nav class="footer_menu">
          <ul>
            <li class="text-uppercase"> <a class="footer_link" href="#">Privacy Policy</a></li>
            <li class="text-uppercase"> <a class="footer_link" href="#">Terms and Conditions</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-sm-4 col-md-3">
        <p class="text-uppercase">Contact US</p>
        <nav class="footer_menu">
          <ul class="d-flex justify-content-center">
            <li class="mr-3"> <a class="footer_link" href="tel:4405 0555"><i class="fas fa-phone-volume"></i>4405 0555</a></li>
            <li> <a class="footer_link" href="#"><i class="fab fa-whatsapp"></i> 7405 0555</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-sm-8 col-md-3">
        <p class="text-uppercase">Follow US</p>
        <ul class="d-flex justify-content-center footer-social"><a class="mr-2 mb-2" href="<?php the_field('facebook',64); ?>"><i class="fab fa-facebook-f"></i></a><a class="mr-2 mb-2" href="<?php the_field('google',64); ?>"><i class="fab fa-google-plus-g"></i></a><a class="mr-2 mb-2" href="<?php the_field('twitter',64); ?>"><i class="fab fa-twitter"></i></a><a class="mr-2 mb-2" href="<?php the_field('instagram',64); ?>"><i class="fab fa-instagram"></i></a></ul>
      </div>
    </div>
  </div>
  <div class="footer_bottom">
    <div class="container d-flex alighn-items-center justify-content-around">
      <p>©Beema, <strong>2019. All Rights Reserved.</strong></p>
      <p> <strong>Made by </strong><a href="https://freezil.com">Freezil.</a></p>
    </div>
  </div>
</footer>
      <div class="loader justify-content-center align-items-center">
            <div id="circularG">
<div id="circularG_1" class="circularG"></div>
<div id="circularG_2" class="circularG"></div>
<div id="circularG_3" class="circularG"></div>
<div id="circularG_4" class="circularG"></div>
<div id="circularG_5" class="circularG"></div>
<div id="circularG_6" class="circularG"></div>
<div id="circularG_7" class="circularG"></div>
<div id="circularG_8" class="circularG"></div>
</div>
      </div>
   <?php wp_footer(); ?>
    </body>
  </html>