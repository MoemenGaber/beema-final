<?php
/* Custom Theme Style */

function beema_theme_scripts() {
    /* Style */
	wp_enqueue_style( 'beema-pluginscss', get_template_directory_uri().'/assets/css/inc/plugins.css' );
	wp_enqueue_style( 'beema-settingcss', get_template_directory_uri().'/assets/css/inc/settings.css' );
	wp_enqueue_style( 'beema-layerscss', get_template_directory_uri().'/assets/css/inc/layers.css' );
	wp_enqueue_style( 'beema-navigationcss', get_template_directory_uri().'/assets/css/inc/navigation.css' );
	wp_enqueue_style( 'beema-custom-style', get_template_directory_uri().'/assets/css/style.css' );
	wp_enqueue_style( 'beema-mo_custom', get_template_directory_uri().'/assets/css/custom_mo.css',99 );

    /* Scripts */
	wp_enqueue_script( 'beema-jquery-plugins', get_template_directory_uri() . '/assets/js/jQuery-plugins.js', array(), '20151215', true );
	wp_enqueue_script( 'beema-jquery-themepunch', get_template_directory_uri() . '/assets/js/jquery.themepunch.tools.min.js', array(), '20151215', true );

	wp_enqueue_script( 'beema-sliderrevolution', get_template_directory_uri() . '/assets/js/jquery.themepunch.revolution.min.js', array(), '20151215', true );
	wp_enqueue_script( 'beema-layeranimation', get_template_directory_uri() . '/assets/js/revolution.extension.layeranimation.min.js', array(), '20151215', true );


    wp_enqueue_script( 'beema-slideanims', get_template_directory_uri() . '/assets/js/revolution.extension.slideanims.min.js', array(), '20151215', true );
    wp_enqueue_script( 'beema-video', get_template_directory_uri() . '/assets/js/revolution.extension.video.min.js', array(), '20151215', true );
    wp_enqueue_script( 'beema-custom-scripts', get_template_directory_uri() . '/assets/js/scripts.js', array(), '20151215', true );

}
add_action( 'wp_enqueue_scripts', 'beema_theme_scripts' );


/* Post Types */

// Register Custom Post Type
function retail_services() {

	$labels = array(
		'name'                  => _x( 'Retail Services', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Retail Services', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Retail Services', 'text_domain' ),
		'name_admin_bar'        => __( 'Retail Services', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New Service', 'text_domain' ),
		'new_item'              => __( 'New Service', 'text_domain' ),
		'edit_item'             => __( 'Edit Service', 'text_domain' ),
		'update_item'           => __( 'Update Service', 'text_domain' ),
		'view_item'             => __( 'View Service', 'text_domain' ),
		'view_items'            => __( 'View Service', 'text_domain' ),
		'search_items'          => __( 'Search Service', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Retail Services', 'text_domain' ),
		'description'           => __( 'Retail Services Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'retail_services', $args );

}
add_action( 'init', 'retail_services', 0 );


// Register Custom Post Type
function corporate_services() {

	$labels = array(
		'name'                  => _x( 'Corporate Services', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Corporate Services', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Corp Services', 'text_domain' ),
		'name_admin_bar'        => __( 'Corporate Services', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New Service', 'text_domain' ),
		'new_item'              => __( 'New Service', 'text_domain' ),
		'edit_item'             => __( 'Edit Service', 'text_domain' ),
		'update_item'           => __( 'Update Service', 'text_domain' ),
		'view_item'             => __( 'View Service', 'text_domain' ),
		'view_items'            => __( 'View Service', 'text_domain' ),
		'search_items'          => __( 'Search Service', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Corporate Services', 'text_domain' ),
		'description'           => __( 'Corporate Services Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'corporate_services', $args );

}
add_action( 'init', 'corporate_services', 0 );


// Register Custom Post Type
function team() {

	$labels = array(
		'name'                  => _x( 'Post Types', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Team', 'text_domain' ),
		'name_admin_bar'        => __( 'Team', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Team', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'team', 0 );

// Register Custom Post Type
function founders() {

	$labels = array(
		'name'                  => _x( 'Founders', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Founders', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Founders', 'text_domain' ),
		'name_admin_bar'        => __( 'Founders', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Founders', 'text_domain' ),
		'description'           => __( 'Founders Descriptions', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'Founders', $args );

}
add_action( 'init', 'founders', 0 );

// Register Custom Post Type
function faq() {

	$labels = array(
		'name'                  => _x( 'FAQs', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'FAQ', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'FAQs', 'text_domain' ),
		'name_admin_bar'        => __( 'FAQ', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'FAQ', 'text_domain' ),
		'description'           => __( 'FAQ', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'FAQ', $args );

}
add_action( 'init', 'faq', 0 );

function Retail_corp_leftAndRight($number) {

if($number%2==1){
	?>
<div class="row margin_top_4">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold"><?php the_title(); ?></h3>
                  <p>
                   <?php the_field('service_excerpt'); ?>
                  </p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_title('Request Page')); ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
				</div>
				<div class="col-lg-6"><img class="w-100" src="<?php the_field('main_service_image_for_the_main_retail_page'); ?>" alt="pic"/></div>

              </div>
						</div>

	<?php
}else{
	?>
  <div class="row margin_top_4">
    <div class="row">
	<div class="col-lg-6"><img class="w-100" src="<?php the_field('main_service_image_for_the_main_retail_page'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
				<h3 class="mt-0 textBlue font-weight-bold"><?php the_title(); ?></h3>
				<p>
				<?php the_field('service_excerpt'); ?>
				</p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_title('Request Page')); ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>

	<?php
}
}


function custom_limit($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}

// Register Custom Post Type
function gallery() {

    $labels = array(
        'name'                  => _x( 'Gallery', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Gallery', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Gallery', 'text_domain' ),
        'name_admin_bar'        => __( 'Gallery', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'attributes'            => __( 'Item Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'view_items'            => __( 'View Items', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Gallery', 'text_domain' ),
        'description'           => __( 'Gallery Description', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'gallery', $args );

}
add_action( 'init', 'gallery', 0 );

function knowMorebtns(){
           if(is_rtl()){
        ?>
        <div class="text-center margin_top"><a class="green_btn btn_style mr-3 mb-3" href="#">تعرف على مزيد من التفاصيل<i class="fas fa-long-arrow-alt-right ml-2"></i></a><a class="green_br_btn btn_style mb-3" href="#">احصل على عرض سعر (مجانا)<i class="fas fa-long-arrow-alt-right ml-2">     </i></a></div>
        <?php
    }else{
        ?>
        <div class="text-center margin_top"><a class="green_btn btn_style mr-3 mb-3" href="#">Know more (Tpl)<i class="fas fa-long-arrow-alt-right ml-2"></i></a><a class="green_br_btn btn_style mb-3" href="#">Get a Quote (FREE)<i class="fas fa-long-arrow-alt-right ml-2">     </i></a></div>
        <?php
    }
  }

  function bannerAndMenuSection($insurance_name,$icon_name,$image_name){
?>

    <section class="topSection banner" style="background: url(<?php echo get_template_directory_uri(  ); ?>/assets/img/<?php echo $image_name; ?>.jpg);">
          <div class="container">
            <div class="text-center top-banner">
              <div><img src="<?php echo get_template_directory_uri(  ); ?>/assets/img/<?php echo $icon_name; ?>.png" alt="alt"/></div>
              <h2>No need to wait for your <?php echo $insurance_name; ?> insurance. Get it now</h2>
              <div><a class="light_btn btn_style mr-2" href="#">Buy Now</a><a class="green_btn btn_style" href="#">Renew</a></div>
			</div>
			 <h1 style="text-align:center; margin-top:20px; margin-bottom:-30px"><?php echo $insurance_name; ?> Insurance</h1>
          </div>
        </section>
          <div class="menu under-banner-menu">
          <nav class="navbar navbar-expand-lg bg-light">
            <div class="container">
              <ul class="navbar-nav m-auto align-items-center justify-content-around">
                <li class="nav-item active"><a class="nav-link" href="#travelInsurance"><?php echo $insurance_name; ?> Insurance</a></li>
                <li class="nav-item"><a class="nav-link" href="#covers">Covers</a></li>
                <li class="nav-item"><a class="nav-link" href="#simpleSteps">Simple Steps</a></li>
              </ul>
            </div>
          </nav>
        </div>

<?php
  }
