<?php

/* Template Name: Motor Insurance */
get_header(); ?>
     <main class="site_main bg">
      <?php bannerAndMenuSection('Motor','motor-icon','motor'); ?>
        <section class="serviceOverview" id="travelInsurance">
          <div class="container">
            <div class="video mb-5"><iframe class="w-100" src="<?php the_field('video_link'); ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>
            <?php the_field('text_under_video'); ?>
            <div class="margin_top_4" id="covers">
              <div class="row">
                <div class="col-lg-6">
               <?php the_field('left_column_text'); ?>
                </div>
                <div class="col-lg-6">
                    <?php the_field('right_column_text'); ?>
                </div>
              </div>
              <div class=" margin_top"></div>
              <div>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/group.jpg" style="display:block; margin:auto; width:100%">
              </div>
              <div class="row margin_top_4" id="simpleSteps">
                <div class="col-lg-6">
                <?php the_field('under_car_left_column_text'); ?>
                </div>
                <div class="col-lg-6">
                 <?php the_field('under_car_left_column_text'); ?>
                </div>
              </div>
           <?php knowMorebtns(); ?>
              <div class="margin_top_4">
                <?php the_field('under_buttons_text'); ?>
              </div>
                <div class="margin_top claims">
                <h3 class="textBlue font-weight-bold">FOLLOW THESE 4 SIMPLE STEPS TO BUY BEEMA’S MOTOR INSURANCE POLICIES:</h3>
                <p>Insuring your vehicle has never been easier! <br> Follow these steps and you’ll be covered in no time—and at affordable rates:</p><img class="mw-100" src="<?php echo get_template_directory_uri(  ); ?>/assets/img/motor-steps.png" alt="alt"/>
              </div>
            </div>
          </div>
        </section>
         <?php if(is_rtl()){
             ?>
             <section class="news pt-0">
                 <div class="container">
                     <h3 class="textDarkBlue _bold">خيارات أخرى</h3>
                     <div class="row">
                         <div class="col-md-6">
                             <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('home-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/home_insurance.jpg" alt="pic"/>
                                     <h3 class="font-weight-bold pr-2 pl-2">تأمين المنزل</h3>
                                     <p class="pr-2 pl-2">اذا كنت تمتلك عقاراً أو تستأجره فلا غني عن تأمين المنزل من بيمه للحفاظ على كافة مقتنياتك وحماية السكان من كافة الأضرار</p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('home-insurance')) ?>">إقرأ المزيد<i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
                         </div>
                         <div class="col-md-6">
                             <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('travel-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/travel-insurance.jpg" alt="pic"/>
                                     <h3 class="font-weight-bold pr-2 pl-2">تأمين السفر</h3>
                                     <p class="pr-2 pl-2">اذا كنت مسافراً لقضاء عطلة مع عائلتك واصدقاءك، أو مسافرأ للعمل، فلما لاتؤمن نفسك بوثيقة تأمين السفر من بيمه؟!</p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('travel-insurance')) ?>">إقرأ المزيد <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
                         </div>
                     </div>
                 </div>
             </section>
         <?php
         }else{
             ?>
             <section class="news pt-0">
                 <div class="container">
                     <h3 class="textDarkBlue _bold">More Options for Retail</h3>
                     <div class="row">
                         <div class="col-md-6">
                             <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('home-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/home_insurance.jpg" alt="pic"/>
                                     <h3 class="font-weight-bold pr-2 pl-2">Home Insurance</h3>
                                     <p class="pr-2 pl-2">Damaan Islamic Insurance Company – Beema was founded in 2009 as a fully Sharia-compliant private closed Qatari shareholding insurance company. Beema offers Takaful </p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('home-insurance')) ?>">Know More <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
                         </div>
                         <div class="col-md-6">
                             <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('travel-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/travel-insurance.jpg" alt="pic"/>
                                     <h3 class="font-weight-bold pr-2 pl-2">Travel Insurance</h3>
                                     <p class="pr-2 pl-2">Audit Committee, Investment Committee Damaan Islamic Insurance Company – Beema was founded in 2009 as a fully Sharia-compliant private closed Qatari</p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('travel-insurance')) ?>">Know More <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
                         </div>
                     </div>
                 </div>
             </section>
         <?php
         }
         ?>

      </main>
   <?php get_footer(); ?>
