<?php 

get_header(); ?>

<main class="site_main">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/group1.jpg);">
          <div class="container">
            <h1>Claims</h1>
            <p class="margin_bottom"><?php the_field('claims_description'); ?></p><a class="d-inline-flex align-items-center call_num" href="tel:<?php the_field('claims_phone_number'); ?>"><i class="fas fa-phone-volume mr-3 textGreen"></i>
              <div><small>Call us for claims</small>
                <p class="textGreen"><?php the_field('claims_phone_number'); ?></p>
              </div></a>
          </div>
        </section>
        <section class="claims">
          <div class="container">
            <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
            <p><?php the_field('claims_process_description'); ?></p>
            <div class="row margin_top">
              <div class="col-sm-6 col-lg-3">
                <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                  <div class="point"><span></span></div>
                  <h3 class="font-weight-bold mb-0"><?php the_field('step_1_title'); ?></h3>
                  <p><?php the_field('step_1_description'); ?></p>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="claim_item text-center lineRight"><i class="far fa-star"></i>
                  <div class="point"><span></span></div>
                  <h3 class="font-weight-bold mb-0"><?php the_field('step_2_title'); ?></h3>
                  <p><?php the_field('step_2_description'); ?></p>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="claim_item text-center lineLeft"><i class="far fa-file-alt"></i>
                  <div class="point"><span></span></div>
                  <h3 class="font-weight-bold mb-0"><?php the_field('step_3_title'); ?></h3>
                  <p><?php the_field('step_3_description'); ?></p>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="claim_item text-center"><i class="fas fa-file-invoice-dollar"></i>
                  <div class="point"><span></span></div>
                  <h3 class="font-weight-bold mb-0"><?php the_field('step_4_title'); ?></h3>
                  <p><?php the_field('step_4_description'); ?></p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="faq pt-0">
          <div class="container">
            <h2 class="textDarkBlue">FAQ</h2>
            <p><?php the_field('faq_section_description'); ?></p>
            <div id="accordion">
              <div class="row">
                <div class="col-lg-6">
                  <div class="card open">
                    <div class="card-header" id="headOne">
                      <h3 class="mb-0">
                        <button class="active" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Question number 01 goes in here?</button>
                      </h3>
                    </div>
                    <div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headTwo">
                      <h3 class="mb-0">
                        <button data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Question number 02 goes in here?</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion">
                      <div class="card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headThree">
                      <h3 class="mb-0">
                        <button data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Question number 02 goes in here?</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-header" id="headFour">
                      <h3 class="mb-0">
                        <button data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">Question number 01 goes in here?</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseFour" aria-labelledby="headingFour" data-parent="#accordion">
                      <div class="card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headFive">
                      <h3 class="mb-0">
                        <button data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">Question number 02 goes in here?</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordion">
                      <div class="card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headSix">
                      <h3 class="mb-0">
                        <button data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">Question number 02 goes in here?</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseSix" aria-labelledby="headingSix" data-parent="#accordion">
                      <div class="card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    <section class="fileClaim pt-0">
        <div class="container">
            <h3 class="textBlue font-weight-bold">Want to File An Insurance Claim ?</h3>
            <p>All businesses involve capital investments. <br> Protection of one’s own investments and that of clients</p>
            <ol class="list margin_bottom">
                <li>Call Us</li>
                <li>Share Details</li>
                <li>We will guide you</li>
            </ol><a class="d-inline-flex align-items-center call_num" href="tel:44050555"><i class="fas fa-phone-volume mr-3 textGreen"></i>
                <div><small>Call us for claims</small>
                    <p class="textGreen">44050555                        </p>
                </div></a>
        </div>
    </section>
      </main>


<?php get_footer();