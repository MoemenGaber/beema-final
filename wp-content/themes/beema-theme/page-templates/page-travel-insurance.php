<?php
/* Template Name: Travel Insurance */
get_header(); ?>

<main class="site_main bg">
                    <?php bannerAndMenuSection('Travel','travel-icon','travel'); ?>
        <section class="serviceOverview">
          <div class="container">
            <h3 class="textDarkBlue" id="travelInsurance"><?php the_field('after_header_title'); ?></h3>
            <p><?php the_field('after_header_description'); ?></p>
            <p class="textBlue font-weight-bold" id="covers">Beema's Travel Insurance Covers:</p>
            <div class="text-center margin_top margin_bottom"><img class="w-75" src="<?php echo get_template_directory_uri(); ?>/assets/img/travel-steps.png" alt="pic"/></div>
            <p class="textBlue font-weight-bold">Travel Insurance </p>
            <p><?php the_field('travel_insurance_description'); ?></p>
            <p class="textBlue font-weight-bold">Follow These Simple Steps To Buy Beema’s Travel Insurance:</p>
            <p><?php the_field('simple_steps_description'); ?></p>
            <div class="text-center margin_top margin_bottom" id="simpleSteps"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/travel-incu.png" alt="pic"/></div>
          </div>
        </section>
        <section class="news pt-0">
          <div class="container">
            <h3 class="textDarkBlue _bold">More Options for Retail</h3>
            <p>Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level four loko PBR.</p>
            <div class="row">
              <div class="col-md-6">
                <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('home-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/home_insurance.jpg" alt="pic"/>
                    <h3 class="font-weight-bold pr-2 pl-2">Home Insurance</h3>
                    <p class="pr-2 pl-2">Irrespective of whether you are renting a house or whether you own one – our Home Insurance policy may be as needed a household item as that “Welcome” mat on your front porch.</p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('home-insurance')) ?>">Know More <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
              </div>
              <div class="col-md-6">
                <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('motor-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/motor_insurance.jpg" alt="pic"/>
                    <h3 class="font-weight-bold pr-2 pl-2">Motor Insurance</h3>
                    <p class="pr-2 pl-2">We offer two types of motor insurance, each with varying levels of protection, and it is important to recognize the inherent differences in the two insurance policies available.</p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('motor-insurance')) ?>">Know More <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
              </div>
            </div>
          </div>
        </section>
      </main>

<?php get_footer(); ?>
