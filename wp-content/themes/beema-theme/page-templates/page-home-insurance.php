<?php
/* Template Name: Home Insurance */


get_header();

?>
      <main class="site_main bg">
              <?php bannerAndMenuSection('Home','home-icom','home'); ?>
          <section class="serviceOverview">
              <div class="container">
                  <div class="video mb-5" id="travelInsurance"><iframe class="w-100" src="<?php the_field('video_link'); ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>
                  <?php the_field('text_under_video'); ?>
                  <p class="textBlue margin_top_4" id="covers">Beema's Home Insurance Covers:</p>
                  <div class="text-center"><img class="w-75" src="<?php echo get_template_directory_uri(); ?>/assets/img/home-covers.png" alt="pic"/></div>
                  <p class="textBlue margin_top_4">Home Insurance</p>
                  <p><?php the_field('home_insurance_description'); ?></p>
                  <p class="textBlue margin_top" id="simpleSteps">Follow These 3 Simple Steps To Buy Beema’s Home Insurance:</p>
                  <p><?php the_field('simple_steps_description'); ?></p>
                  <div class="text-center margin_top"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/branches.png" alt="pic"/></div>
              </div>
          </section>
        <section class="news pt-0">
          <div class="container">
            <h3 class="textDarkBlue _bold">More Options for Retail</h3>
            <p>Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level four loko PBR.</p>
            <div class="row">
              <div class="col-md-6">
                <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('motor-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/motor_insurance.jpg" alt="pic"/>
                    <h3 class="font-weight-bold pr-2 pl-2">Motor Insurance</h3>
                    <p class="pr-2 pl-2">We offer two types of motor insurance, each with varying levels of protection, and it is important to recognize the inherent differences in the two insurance policies available.</p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('motor-insurance')) ?>">Know More <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
              </div>
              <div class="col-md-6">
                <div class="blog_item mt-4"><a href="<?php echo get_permalink(get_page_by_path('travel-insurance')) ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/travel-insurance.jpg" alt="pic"/>
                    <h3 class="font-weight-bold pr-2 pl-2">Travel Insurence</h3>
                    <p class="pr-2 pl-2">Whenever you’re traveling – be it with your friends and family for vacations or for business-related reasons – it’s a great idea to buy travel insurance!</p><a class="textBlue pr-2 pl-2" href="<?php echo get_permalink(get_page_by_path('travel-insurance')) ?>">Know More <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
              </div>
            </div>
          </div>
        </section>
      </main>
<?php
get_footer();
