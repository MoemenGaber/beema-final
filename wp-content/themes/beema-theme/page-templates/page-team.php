<?php 
/* Template Name: Team */

get_header(); ?>

<main class="site_main">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/group1.jpg)">
          <div class="container">
            <h1>Team Members / Board </h1>
            <p>Damaan Islamic Insurance Company – Beema was founded in 2009 as a fully Sharia-compliant private closed Qatari shareholding insurance company. Beema offers Takaful solutions that provide protection across a wide spectrum of risk categories.</p>
          </div>
        </section>
        <section class="team">
          <div class="container">
            <div class="row">
              <div class="col-lg-3">
                <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headOne" target="#tabZero">
                      <h3 class="mb-0">
                        <button class="active" data-toggle="collapse" data-target="#collapseZero" aria-expanded="true" aria-controls="collapseZero">Company Overview</button>
                      </h3>
                    </div>
                    <div class="collapse show" id="collapseZero" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="card-body">
                        <p><?php the_field('board_of_directories_description'); ?></p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headOne" target="#tabOne">
                      <h3 class="mb-0">
                        <button class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Board Of Directors</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="card-body">
                        <p><?php the_field('board_of_directories_description'); ?></p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headOTow" target="#tabTow">
                      <h3 class="mb-0">
                        <button data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Sharia’ Supervisory Board</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion">
                      <div class="card-body">
                        <p><?php the_field('sharia’_supervisory_board'); ?></p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headThree" target="#tabThree">
                      <h3 class="mb-0">
                        <button data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Management Team</button>
                      </h3>
                    </div>
                    <div class="collapse" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                        <p><?php the_field('management_team'); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-9">
              <!-- Company Overview -->
              
              <div class="tab animated fadeIn show" id="tabZero">
                <div class="row">
                <div class="container">
                <h1 style="text-align:center">Company Overview</h1>
            </div>
                
            
        <section class="clients pb-0">
            <div class="container">
                <p class="textGreen">The founders of the company are</p>
            <div class="logos owl-carousel margin_top margin_bottom">

            <?php
// The Loop
	while (have_rows('founders',3) ) {
	the_row();
    ?>
            <div class="logo"><img src="<?php the_sub_field('image',3); ?>" alt="pic"/></div>

    <?php }
    ?>


            </div>

          </div>
        </section>
    <section class="overview pt-4">
        <div class="container">
            <h3 class="textBlue margin_bottom">Takaful History</h3>
            <p><?php the_field('description',3); ?></p>
            <div class="video mb-5 mt-5"><iframe class="w-100" src="<?php the_field('video_link',3); ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                          </div>
            <h3 class="textBlue margin_bottom">Principles</h3>
           <p><?php the_field('principles_description',3); ?></p>
        </div>
    </section>
      

                </div>
                </div>
                <div class="tab animated fadeIn" id="tabOne">
                <div class="row">
                                    <?php
                            // WP_Query arguments
                    $args = array(
                        'post_type'              => array( 'team' ),
                        'order' =>'ASC',
                        'category__in' => array('1'),
                    );

                    // The Query
                    $team = new WP_Query( $args );
                  
                    // The Loop
                    if ( $team->have_posts() ) {
                        while ( $team->have_posts() ) {
                            $team->the_post();
                            ?>
                
                    <div class="col-sm-6 col-md-4">
                      <div class="blog_item margin_bottom"><img class="margin_bottom" src="<?php the_field('image'); ?>" alt="pic"/>
                          <p class="font-weight-bold mb-0"><?php the_title(); ?></p><small class="textBlue d-block mb-1"><?php the_field('position'); ?></small>
                          <p><?php the_field('information'); ?></p></div>
                    </div>
                  
                        <?php  } }
                        wp_reset_postdata();
                        ?>

                </div>
                </div>
                <div class="tab animated fadeIn" id="tabTow">
                <div class="row">
                                    <?php
                            // WP_Query arguments
                    $args = array(
                        'post_type'              => array( 'team' ),
                        'order' =>'ASC',
                        'category__in' => array('2'),
                    );

                    // The Query
                    $team = new WP_Query( $args );
                  
                    // The Loop
                    if ( $team->have_posts() ) {
                        while ( $team->have_posts() ) {
                            $team->the_post();
                            ?>
                
                    <div class="col-sm-6 col-md-4">
                      <div class="blog_item margin_bottom"><img class="margin_bottom" src="<?php the_field('image'); ?>" alt="pic"/>
                          <p class="font-weight-bold mb-0"><?php the_title(); ?></p><small class="textBlue d-block mb-1"><?php the_field('position'); ?></small>
                          <p><?php the_field('information'); ?></p></div>
                    </div>
                  
                        <?php  } }
                        wp_reset_postdata();
                        ?>

                </div></div>
                <div class="tab animated fadeIn" id="tabThree">
                <div class="row">
                                    <?php
                            // WP_Query arguments
                    $args = array(
                        'post_type'              => array( 'team' ),
                        'order' =>'ASC',
                        'category__in' => array('3'),
                    );

                    // The Query
                    $team = new WP_Query( $args );
                  
                    // The Loop
                    if ( $team->have_posts() ) {
                        while ( $team->have_posts() ) {
                            $team->the_post();
                            ?>
                
                    <div class="col-sm-6 col-md-4">
                      <div class="blog_item margin_bottom"><img class="margin_bottom" src="<?php the_field('image'); ?>" alt="pic"/>
                          <p class="font-weight-bold mb-0"><?php the_title(); ?></p><small class="textBlue d-block mb-1"><?php the_field('position'); ?></small>
                          <p><?php the_field('information'); ?></p></div>
                    </div>
                  
                        <?php  } }
                        wp_reset_postdata();
                        ?>

                </div>
                </div>
              
              </div>
            <!-- END Company overview -->
            </div>

        </section>
      </main>

<?php get_footer();