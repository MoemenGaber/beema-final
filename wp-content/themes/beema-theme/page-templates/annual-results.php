<?php
/* Template Name: Annual Results */
get_header(); ?>

   <main class="site_main bg">
   <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/investors/annual-results/header.jpg);">
          <div class="container">
            <h1>Annual Results</h1>
          </div>
        </section>
        <section class="text-center">
          <div class="container">
            <div class="row">
                <?php while(have_rows('annual_result')){ the_row(); ?>
             <div class="col-sm-6 col-md-4 col-lg-3"> <a href="<?php the_sub_field('link'); ?>"><img class="mw-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></a></div>
        <?php } ?>
    </div>
            <h3 class="textBlue mt-5 text-center">To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>
<?php get_footer();
