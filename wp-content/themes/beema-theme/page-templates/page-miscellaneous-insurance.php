<?php 

/* Template Name: Micellaneous Insurance */

get_header(); ?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/miscellaneous-Insurance.jpg);">
          <div class="container">
            <h1>Miscellaneous Insurance</h1>
          </div>
        </section>
        <section class="insurance">
          <div class="container">
            <div class="text-center">
              <p class="w-75 textBlue m-auto"><?php the_field('under_header_description'); ?></p>
              <div class="row margin_top inc-steps">
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/hotel-inc.png" alt="pic"/></div>
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/cancelation-inc.png" alt="pic"/></div>
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/jewelers-inc.png" alt="pic"/></div>
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/event-inc.png" alt="pic"/></div>
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/baggage-inc.png" alt="pic"/></div>
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/bankers-inc.png" alt="pic"/></div>
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/burglary-inc.png" alt="pic"/></div>
                <div class="col-lg-3 col-sm-6 margin_bottom"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/personal-inc.png" alt="pic"/></div>
              </div>
            </div>
            <p class="textBlue mt-5">BANKERS BLANKET BOND (BBB)</p>
            <p><?php the_field('bankers_description'); ?></p>
            <p class="textBlue">HOTEL COMPREHENSIVE INSURANCE</p>
            <p><?php the_field('hotel_description'); ?></p>
            <p class="textBlue">JEWELERS BLOCK INSURANCE</p>
            <p><?php the_field('jewelers_description'); ?></p>
            <p class="textBlue">BAGGAGE ALL RISKS INSURANCE</p>
            <p><?php the_field('baggage_description'); ?></p>
            <p class="textBlue">EVENT CANCELATION</p>
            <p><?php the_field('event_description'); ?></p>
            <p class="textBlue">CONTINGENCY/CANCELLATION INSURANCE</p>
            <?php the_field('contingency_description'); ?>
            <p class="textBlue">OTHER INSURANCE POLICIES</p>
            <?php the_field('other_description'); ?>
            <h3 class="mt-5 textBlue">To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>

<?php get_footer();