<?php 
/* Template Name: Fire Insurance */


get_header();
?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/fire.jpg);">
          <div class="container">
            <h1>Property Insurance</h1>
          </div>
        </section>
        <section class="insurance">
          <div class="container">
            <p><?php the_field('under_header_description'); ?></p>
            <h3 class="textBlue">FIRE & SPECIAL PERILS INSURANCE   (INCLUDING BURGLARY)</h3>
            <p><?php the_field('special_perils_description'); ?></p>
            <h3 class="textBlue">PROPERTY ALL RISKS INSURANCE</h3>
            <p><?php the_field('property_description'); ?></p>
            <div class="text-center margin_top_4">
              <h3 class="textGreen">Fire & Property Insurance</h3>
              <h2 class="textGreen">Quotation Checklist</h2>
              <h3>to receive a quote for our Property All Risks Insurance, we require estmaied values for the following:</h3><img class="w-75" src="<?php echo get_template_directory_uri();?>/assets/img/fire-steps.png" alt="pic"/>
            </div>
            <h3 class="mt-5 textBlue">BUSINESS INTERRUPTION INSURANCE</h3>
              <p><?php the_field('business_description'); ?></p>
          </div>
        </section>
      </main>

<?php
get_footer();