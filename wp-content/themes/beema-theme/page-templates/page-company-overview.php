<?php
/* Template Name: Company Overview */
get_header();
?>

    <main class="site_main">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/img/overview.jpg);">
            <div class="container">
                <h1>Company Overview</h1>
            </div>
        </section>
        <section class="clients pb-0">
            <div class="container">
                <p class="textGreen">The founders of the company are</p>
            <div class="logos owl-carousel margin_top margin_bottom">

            <?php
// The Loop
	while (have_rows('founders') ) {
	the_row();
    ?>
            <div class="logo"><img src="<?php the_sub_field('image'); ?>" alt="pic"/></div>

    <?php }
    ?>


            </div>

          </div>
        </section>
    <section class="overview pt-4">
        <div class="container">
            <h3 class="textBlue margin_bottom">Takaful History</h3>
            <p><?php the_field('description'); ?></p>
            <div class="video mb-5 mt-5"><iframe class="w-100" src="<?php the_field('video_link'); ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                          </div>
            <h3 class="textBlue margin_bottom">Principles</h3>
           <p><?php the_field('principles_description'); ?></p>
        </div>
    </section>
      </main>


<?php
get_footer();
