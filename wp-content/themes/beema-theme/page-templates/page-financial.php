<?php 
/* Template Name: Financial */


get_header();
?>
  <main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/financials.jpg);">
          <div class="container">
            <h1>Financials</h1>
          </div>
        </section>
        <section class="text-center">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial1.png" alt="pic"/></div>
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial2.png" alt="pic"/></div>
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial3.png" alt="pic"/></div>
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial4.png" alt="pic"/></div>
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial5.png" alt="pic"/></div>
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial6.png" alt="pic"/></div>
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial7.png" alt="pic"/></div>
              <div class="col-sm-6 col-md-4 col-lg-3"><img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/financial8.png" alt="pic"/></div>
            </div>
            <h3 class="textBlue mt-5 text-center">To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>

<?php
get_footer();