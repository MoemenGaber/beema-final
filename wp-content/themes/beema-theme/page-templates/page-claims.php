<?php 

/* Template Name: Claims page */

get_header(); ?>

<main class="site_main claims">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(  ); ?>/assets/img/claims.jpg);">
            <div class="container">
                <h1>Claims</h1>
            </div>
        </section>
        <section class="contact-us">
            <div class="container">
                <!-- <h2 class="text-center textBlue light">We are Happy To Server You !</h2> -->
            </div>
        </section>
        <section class="branches pt-0">
            <div class="container">
                <div class="maintaps">
                    <ul class="nav-tabs justify-content-center pb-3">
                        <li><a class="nav-link active _main" href="#branches">Retail</a></li>
                        <li><a class="nav-link _main" href="#selfServiceMachines">Corportate</a></li>
                    </ul>
                    <div class="tap animated fadeIn active pt-3" id="branches">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" id="al-sadd-b" data-toggle="tab" href="#al-sadd" role="tab" aria-controls="al-sadd" aria-selected="true">Motor Insurance</a></li>
                            <li><a class="nav-link" id="abo-mahmour-b" data-toggle="tab" href="#abo-mahmour" role="tab" aria-controls="abo-mahmour" aria-selected="false">Travel Insurance</a></li>
                            <li> <a class="nav-link" id="al-mirqab-b" data-toggle="tab" href="#al-mirqab" role="tab" aria-controls="al-mirqab" aria-selected="false">Home Insurance</a></li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade claims active show" id="al-sadd" role="tabpanel" aria-labelledby="al-sadd">
            <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
            <?php while(have_rows('retail')){ the_row(); ?> 
            <p><?php the_sub_field('motor_process_description'); ?></p>
            <div class="row margin_top">
            <?php $id=1; ?>
              <?php while(have_rows('motor_steps')){ the_row(); ?>
                <div class="col-sm-6 col-lg-3">
                <div class="claim_item text-center <?php if($id > 1){ echo "lineRight"; } ?>"><i class="far fa-star"></i>
                  <div class="point"><span></span></div>
                  <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?></h3>
                 <p><?php the_sub_field('description'); ?></p>
                </div>
              </div>
                <?php 
              $id++;
              } ?>
          
            </div>
          </div>
          <div class="tab-pane fade claims " id="abo-mahmour" role="tabpanel" aria-labelledby="abo-mahmour">
          <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
            <p><?php the_sub_field('travel_process_description'); ?></p>
            <div class="row margin_top">
            <?php $id=1; ?>
              <?php while(have_rows('travel_steps')){ the_row(); ?>
                <div class="col-sm-6 col-lg-3">
                <div class="claim_item text-center <?php if($id > 1){ echo "lineRight"; } ?>"><i class="far fa-star"></i>
                  <div class="point"><span></span></div>
                  <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?></h3>
                  <p><?php the_sub_field('description'); ?></p>
                </div>
              </div>
              <?php
              $id++;
            } ?>
            </div>
          </div>
          <div class="tab-pane fade claims" id="al-mirqab" role="tabpanel" aria-labelledby="al-mirqab">
          <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
            <p><?php the_sub_field('home_process_description'); ?></p>
            <div class="row margin_top">
              <?php while(have_rows('home_steps')){ the_row(); ?>
                <div class="col-sm-6 col-lg-3">
                <div class="claim_item text-center <?php if($id > 1){ echo "lineRight"; } ?>"><i class="far fa-star"></i>
                  <div class="point"><span></span></div>
                  <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?></h3>
                  <p><?php the_sub_field('description'); ?></p>
                </div>
              </div>
              <?php 
            $id++;
            } ?>
            </div>
          </div>
            <?php } ?>
                        
                        </div>
                    </div>
            <?php while(have_rows('corporate')){ the_row(); ?>
                    <div class="tap animated fadeIn pt-3" id="selfServiceMachines">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" id="machine1" data-toggle="tab" href="#_machine1" role="tab" aria-controls="_machine1" aria-selected="true">Fire and Property Insurance</a></li>
                            <li><a class="nav-link" id="machine2" data-toggle="tab" href="#_machine2" role="tab" aria-controls="_machine2" aria-selected="false">Liability Insurance</a></li>
                            <li><a class="nav-link" id="machine3" data-toggle="tab" href="#_machine3" role="tab" aria-controls="_machine3" aria-selected="false">Engineering Insurance</a></li>
                            <li><a class="nav-link" id="machine4" data-toggle="tab" href="#_machine4" role="tab" aria-controls="_machine4" aria-selected="false">Miscellaneous Insurance</a></li>
                            <li><a class="nav-link" id="machine5" data-toggle="tab" href="#_machine5" role="tab" aria-controls="_machine5" aria-selected="false">Marine Insurance</a></li>
                            <li><a class="nav-link" id="machine6" data-toggle="tab" href="#_machine6" role="tab" aria-controls="_machine6" aria-selected="false">Aviation Insurance</a></li>
                            <li><a class="nav-link" id="machine7" data-toggle="tab" href="#_machine7" role="tab" aria-controls="_machine7" aria-selected="false">Group life Insurance</a></li>
                            <li><a class="nav-link" id="machine8" data-toggle="tab" href="#_machine8" role="tab" aria-controls="_machine8" aria-selected="false">Group Medical Insurance</a></li>
                            <li><a class="nav-link" id="machine9" data-toggle="tab" href="#_machine9" role="tab" aria-controls="_machine9" aria-selected="false">Workmen’s Compensation Insurance</a></li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
            <!-- Start Corp tab -->
                      <div class="tab-pane fade claims active show " id="_machine1" role="tabpanel" aria-labelledby="_machine1">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('property_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('property_steps')){ the_row(); ?>
                  <div class="col-sm-6 col-lg-3">
                    <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                      <div class="point"><span></span></div>
                      <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?></h3>
                      <p><?php the_sub_field('description'); ?></p>
                    </div>
                  </div>
                    <?php } ?>
                </div>
              </div>
            <!-- End Corp tab -->

            <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine2" role="tabpanel" aria-labelledby="_machine2">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('liability_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('liability_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

            <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine3" role="tabpanel" aria-labelledby="_machine3">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('engineering_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('engineering_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

            <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine4" role="tabpanel" aria-labelledby="_machine4">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('miscellaneous_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('miscellaneous_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

            <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine5" role="tabpanel" aria-labelledby="_machine5">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('marine_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('marine_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

                    <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine6" role="tabpanel" aria-labelledby="_machine6">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('aviation_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('aviation_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

                    <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine7" role="tabpanel" aria-labelledby="_machine7">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('group_life_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('group_life_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

                <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine8" role="tabpanel" aria-labelledby="_machine8">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('group_medical_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('group_medical_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

                <!-- Start Corp tab -->
            <div class="tab-pane fade claims " id="_machine9" role="tabpanel" aria-labelledby="_machine9">
                <h3 class="font-weight-bold textDarkBlue">Claims process</h3>
                <p><?php the_sub_field('workmen_process_description'); ?></p>
                <div class="row margin_top">
                    <?php while(have_rows('workmen_medical_steps')){ the_row(); ?>
                        <div class="col-sm-6 col-lg-3">
                            <div class="claim_item text-center"><i class="far fa-file-alt"></i>
                                <div class="point"><span></span></div>
                                <h3 class="font-weight-bold mb-0"><?php the_sub_field('title'); ?>></h3>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Corp tab -->

                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="faq pt-0">
          <div class="container">
            <h2 class="textDarkBlue">FAQ</h2>
            <p><?php the_field('faq_description'); ?></p>
            <div id="accordion">
              <div class="row">
                <div class="col-lg-6">
                    <?php
                    // WP_Query arguments
                    $args = array(
                        'post_type'              => array( 'faq' ),
                        'order'=>'ASC',
                    );

                    // The Query
                    $faq = new WP_Query( $args );
                    $faq_number=$faq->post_count;

                    // The Loop
                    if ( $faq->have_posts() ) {
                        while ( $faq->have_posts() ) {
                            $faq->the_post();
                           ?>
                            <div class="card">
                                <div class="card-header" id="headOne">
                                    <h3 class="mb-0">
                                        <button class="" data-toggle="collapse" data-target="#<?php echo the_ID(); ?>" aria-expanded="true" aria-controls="2"><?php the_title(); ?></button>
                                    </h3>
                                </div>
                                <div class="collapse" id="<?php echo the_ID(); ?>" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo the_content(); ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }

                    // Restore original Post Data
                    wp_reset_postdata();


                    ?>


                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="fileClaim pt-0">
          <div class="container">
            <h3 class="textBlue font-weight-bold">Want to File An Insurance Claim ?</h3>
           <?php the_field('description'); ?>
            <a class="d-inline-flex align-items-center call_num" href="tel:44050555"><i class="fas fa-phone-volume mr-3 textGreen"></i>
              <div><small>Call us for claims</small>
                <p class="textGreen">44050555                        </p>
              </div></a>
              <a class="d-inline-flex align-items-center call_num" href="tel:44050555"><i class="fas fa-globe-asia mr-3 textGreen"></i>
              <div><small>Apply your claim online</small>
                <p class="textGreen">Coming soon                       </p>
              </div></a>
          </div>
        </section>
    </main>
<?php get_footer();