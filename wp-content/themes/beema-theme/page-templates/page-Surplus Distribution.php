<?php 
/* Template Name: Surplus Distribution */


get_header();
?>

<main class="site_main bg">
<section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/investors/surplus-distruptions/header.jpg);">
          <div class="container">
            <h1>Surplus Distribution</h1>
          </div>
        </section>
    <section class="text-center credit">
        <div class="container">
            <div class="row">
                <?php while(have_rows('surplus_distribution')){  the_row(); ?>
                    <div class="col-md-6">
                        <a href="<?php the_sub_field('link'); ?>">
                        <div class="__item d-flex align-items-center mb-5">
                            <h3 class="text-capitalize textBlue mb-0"><?php the_sub_field('title'); ?></h3>
                            <h3 class="_date"><?php the_sub_field('year'); ?></h3>
                        </div>
                        </a>
                    </div>
                <?php } ?>
            </div>

        </div>
    </section>
      </main>
<?php
get_footer();