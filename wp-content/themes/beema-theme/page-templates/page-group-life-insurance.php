<?php 
/* Template Name: Group life Insurance */


get_header();
?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/group-life-insurance.jpg);">
          <div class="container">
            <h1>Group Life Insurance</h1>
          </div>
        </section>
        <section>
          <div class="container">
            <p><?php the_field('under_header_description'); ?></p>
            <img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/group-life-steps.png" alt="pic"/>
            <p><?php the_field('under_image_description'); ?></p>
            <h3 class="textBlue mt-5">To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>

<?php
get_footer();