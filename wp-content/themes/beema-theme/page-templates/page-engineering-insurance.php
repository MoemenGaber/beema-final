<?php 
/* Template Name: Engineering Insurance */


get_header();

?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/engineering.jpg);">
          <div class="container">
            <h1>Engineering Insurance</h1>
          </div>
        </section>
        <section class="insurance">
          <div class="container">
            <p><?php the_field('under_header_description'); ?></p>
            <h3 class="textBlue">CONTRACTORS ALL RISKS INSURANCE</h3>
            <p><?php the_field('contractors_description'); ?></p>
            <h3 class="textBlue mt-4">ERECTION ALL RISK INSURANCE</h3>
            <p><?php the_field('erection_description'); ?></p>
            <h3 class="textBlue mt-4">ANNUAL CONTRACT ALL RISK INSURANCE</h3>
            <p><?php the_field('annual_description'); ?></p>
            <h3 class="textBlue mt-4">PLANT & EQUIPMENT INSURANCE</h3>
            <p><?php the_field('plant_description'); ?></p>
            <h3 class="textBlue mt-4">ELECTRONIC EQUIPMENT BREAKDOWN INSURANCE</h3>
            <p><?php the_field('electronic_description'); ?></p>
            <h3 class="textBlue mt-4"> To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>

<?php
get_footer();