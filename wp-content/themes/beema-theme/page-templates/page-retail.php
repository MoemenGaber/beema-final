<?php

/* Template Name: Retail */
get_header(); ?>

<main class="site_main">
        <section class="topSection" style="background: url(<?php the_field('header_background_image'); ?>);">
          <div class="container">
            <h1>Retail Insurance</h1>
            <p><?php the_field('retail_page_description'); ?></p>
          </div>
        </section>
  <section class="serviceOverview">
          <div class="container">
            <h2 class="textDarkBlue">Service Overview</h2>
            <p><?php the_field('service_overview_description'); ?></p>
            <div class="margin_top_4">
            <?php while(have_rows('motor_insurance')){
                    the_row();
                ?>
              <div class="row">
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
              
                  <h3 class="mt-0 textBlue font-weight-bold">Motor Insurance</h3>
                  <p>
                   <?php the_sub_field('description'); ?>
                  </p>
                  <?php } ?>
                  <a class="green_btn btn_style" href="http://freezil.com/demo/beema/motor-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <?php while(have_rows('travel_insurance')){
                the_row();
                ?>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Travel Insurance</h3>
                  <p>
                    <?php the_sub_field('description'); ?>
                  </p>
                  
                  <a class="green_btn btn_style" href="http://freezil.com/demo/beema/travel-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
              </div>
              <?php } ?>
              <?php while(have_rows('home_insurance')){ 
                  the_row();
                ?>
              <div class="row margin_top_4">
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Home Insurance</h3>
                  <p>
                   <?php the_sub_field('description'); ?>
                  </p>
                  <a class="green_btn btn_style" href="http://freezil.com/demo/beema/home-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </section>
      </main>

<?php get_footer(); ?>
