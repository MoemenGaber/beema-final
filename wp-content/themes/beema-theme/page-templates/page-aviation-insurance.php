<?php 
/* Template Name: Aviation Insurance */


get_header();
?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/aviation.jpg);">
          <div class="container">
              <?php if(is_rtl()){
                 ?>
    <h1>التأمين الجوي</h1>
    <?php
              }else{
              ?>
            <h1>Aviation Insurance</h1>
         <?php } ?>
          </div>
        </section>
        <section>
          <div class="container">
            <p class="margin_bottom_4"><?php the_field('text_above_image'); ?></p>
              <img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/aviation-steps.png" alt="pic"/>
            <?php the_field('text_under_image'); ?>
          </div>
        </section>
      </main>

<?php
get_footer();