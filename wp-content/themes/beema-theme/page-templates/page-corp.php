<?php

/* Template Name: Corporate */
get_header(); ?>

<main class="site_main">
        <section class="topSection" style="background: url(<?php the_field('header_background_image'); ?>);">
          <div class="container">
            <h1>Corporate Insurance</h1>
            <p><?php the_field('corp_page_description'); ?></p>
          </div>
        </section>
  <section class="serviceOverview">
          <div class="container">
            <h2 class="textDarkBlue">Service Overview</h2>
            <p><?php the_field('service_overview_description'); ?></p>
            <div class="margin_top_4">
            <?php while(have_rows('fire_and_property_insurance')){ the_row();
              ?>
              <div class="row">
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Fire And Property Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
              </p>
              <a class="green_btn btn_style" href="http://freezil.com/demo/beema/fire-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              
              </div>
              <?php } ?>
              <?php if(have_rows('liability_insurance')){  the_row(); ?>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Liability Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                 </p>
                 
                 <a class="green_btn btn_style" href="http://freezil.com/demo/beema/liability-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
              </div>
              <?php } ?>
              <?php while(have_rows('engineering_insurance')){ the_row();
                ?>
              <div class="row margin_top_4">
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Engineering Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                  </p><a class="green_btn btn_style" href="http://freezil.com/demo/beema/engineering-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <?php } ?>
              <?php while(have_rows('miscellaneous_insurance')){
                the_row();
                ?>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Miscellaneous Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                  </p><a class="green_btn btn_style" href="http://freezil.com/demo/beema/miscellaneous-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
              </div>
              <?php } ?>
              <?php while(have_rows('workmen_compensation_insurance')){
                the_row();
                ?>
              <div class="row margin_top_4">
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Workmen’s Compensation Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                  </p><a class="green_btn btn_style" href="http://freezil.com/demo/beema/workmens-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <?php } ?>
              <?php while(have_rows('marine_hull_insurance')){
                the_row();
                ?>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Marine Hull Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                  </p><a class="green_btn btn_style" href="http://freezil.com/demo/beema/marine-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
              </div>
              <?php } ?>
              <?php while(have_rows('aviation_insurance')){
                the_row();
                ?>
              <div class="row margin_top_4">
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Aviation Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                  </p>
                  <a class="green_btn btn_style" href="http://freezil.com/demo/beema/aviation-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <?php } ?>
              <?php while(have_rows('group_life_insurance')){
                the_row();
                ?>
              
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Group Life Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                  </p>
                 <a class="green_btn btn_style" href="http://freezil.com/demo/beema/group-life-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
              </div>
              <?php } ?>
              <?php while(have_rows('group_medical_insurance')){
                the_row();
                ?>
              <div class="row margin_top_4">
                <div class="col-lg-6"><img class="w-100" src="<?php the_sub_field('image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold">Group Medical Insurance</h3>
                  <p>
                  <?php the_sub_field('description'); ?>
                  </p><a class="green_btn btn_style" href="http://freezil.com/demo/beema/group-medical-insurance/">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </section>

      </main>

<?php get_footer(); ?>
