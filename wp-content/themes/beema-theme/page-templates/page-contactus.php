<?php

/* Template Name: Contact Us */

get_header();
?>
    <main class="site_main">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(  ); ?>/assets/img/contact_us.jpg);">
            <div class="container">
                <h1>Contact Us</h1>
            </div>
        </section>
        <section class="contact-us">
            <div class="container">
                <h2 class="text-center textBlue light">We are Happy To Serve You !</h2>
            </div>
        </section>
        <section class="branches pt-0">
            <div class="container">
                <div class="maintaps">
                    <ul class="nav-tabs justify-content-center pb-3">
                        <li><a class="nav-link active _main" href="#branches">Branches</a></li>
                        <li><a class="nav-link _main" href="#selfServiceMachines">Self Service Machines</a></li>
                    </ul>
                    <div class="tap animated fadeIn active pt-3" id="branches">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" id="al-sadd-b" data-toggle="tab" href="#al-sadd" role="tab" aria-controls="al-sadd" aria-selected="true">AL SADD BRANCH</a></li>
                            <li><a class="nav-link" id="abo-mahmour-b" data-toggle="tab" href="#abo-mahmour" role="tab" aria-controls="abo-mahmour" aria-selected="false">ABU HAMOUR BRANCH</a></li>
                            <li> <a class="nav-link" id="al-mirqab-b" data-toggle="tab" href="#al-mirqab" role="tab" aria-controls="al-mirqab" aria-selected="false">AL MIRQAB BRANCH</a></li>
                            <li><a class="nav-link" id="al-khor-b" data-toggle="tab" href="#al-khor" role="tab" aria-controls="al-khor" aria-selected="false">Al Khor BRANCH</a></li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="al-sadd" role="tabpanel" aria-labelledby="al-sadd-b">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">AL SADD BRANCH :</p>
                                        <p>Timing : (9 AM To 2 PM )(Sunday To Thursday) <br></p>HEAD OFFICE
                                        <p class="textGreen mb-0">Office Timing</p>
                                        <p>Sun to Thurs :  9am to 2pm Friday & Saturday : CLOSED</p>
                                        <p class="textGreen mb-0">Address</p>
                                        <p class="mb-0">Al Sadd , Suhaim Bin Hamad Street, Qatar First Bank Building ,</p>
                                        <p class="mt-0">8th floor P.O. Box: 11068, Doha, State of Qatar.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">Phone Number</p>
                                        <p><a href="tel:974-44050555">974-44050555</a></p>
                                        <p class="textGreen mb-0">Fax</p>
                                        <p> 974-44050505 </p>
                                        <p class="textGreen mb-0">Email</p>
                                        <p> <a href="mailto:info@beema.com.qa">info@beema.com.qa</a></p>
                                    </div>
                                </div>
                                <div class="map margin_top"><iframe class="w-100" height="300" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3411.111071138104!2d29.965093864623746!3d31.24534841779793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssan+stefano+mall!5e0!3m2!1sen!2seg!4v1556046470452!5m2!1sen!2seg" allowfullscreen></iframe></div>
                            </div>
                            <div class="tab-pane fade" id="abo-mahmour" role="tabpanel" aria-labelledby="abo-mahmour-b">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">AL SADD BRANCH :</p>
                                        <p>Timing : (9 AM To 2 PM )(Sunday To Thursday) <br></p>HEAD OFFICE
                                        <p class="textGreen mb-0">Office Timing</p>
                                        <p>Sun to Thurs :  9am to 2pm Friday & Saturday : CLOSED</p>
                                        <p class="textGreen mb-0">Address</p>
                                        <p class="mb-0">Al Sadd , Suhaim Bin Hamad Street, Qatar First Bank Building ,</p>
                                        <p class="mt-0">8th floor P.O. Box: 11068, Doha, State of Qatar.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">Phone Number</p>
                                        <p><a href="tel:974-44050555">974-44050555</a></p>
                                        <p class="textGreen mb-0">Fax</p>
                                        <p> 974-44050505 </p>
                                        <p class="textGreen mb-0">Email</p>
                                        <p> <a href="mailto:info@beema.com.qa">info@beema.com.qa</a></p>
                                    </div>
                                </div>
                                <div class="map margin_top"><iframe class="w-100" height="300" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3411.111071138104!2d29.965093864623746!3d31.24534841779793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssan+stefano+mall!5e0!3m2!1sen!2seg!4v1556046470452!5m2!1sen!2seg" allowfullscreen></iframe></div>
                            </div>
                            <div class="tab-pane fade" id="al-mirqab" role="tabpanel" aria-labelledby="al-mirqab-b">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">AL SADD BRANCH :</p>
                                        <p>Timing : (9 AM To 2 PM )(Sunday To Thursday) <br></p>HEAD OFFICE
                                        <p class="textGreen mb-0">Office Timing</p>
                                        <p>Sun to Thurs :  9am to 2pm Friday & Saturday : CLOSED</p>
                                        <p class="textGreen mb-0">Address</p>
                                        <p class="mb-0">Al Sadd , Suhaim Bin Hamad Street, Qatar First Bank Building ,</p>
                                        <p class="mt-0">8th floor P.O. Box: 11068, Doha, State of Qatar.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">Phone Number</p>
                                        <p><a href="tel:974-44050555">974-44050555</a></p>
                                        <p class="textGreen mb-0">Fax</p>
                                        <p> 974-44050505 </p>
                                        <p class="textGreen mb-0">Email</p>
                                        <p> <a href="mailto:info@beema.com.qa">info@beema.com.qa</a></p>
                                    </div>
                                </div>
                                <div class="map margin_top"><iframe class="w-100" height="300" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3411.111071138104!2d29.965093864623746!3d31.24534841779793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssan+stefano+mall!5e0!3m2!1sen!2seg!4v1556046470452!5m2!1sen!2seg" allowfullscreen></iframe></div>
                            </div>
                            <div class="tab-pane fade" id="al-khor" role="tabpanel" aria-labelledby="al-khor-b">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">AL SADD BRANCH :</p>
                                        <p>Timing : (9 AM To 2 PM )(Sunday To Thursday) <br></p>HEAD OFFICE
                                        <p class="textGreen mb-0">Office Timing</p>
                                        <p>Sun to Thurs :  9am to 2pm Friday & Saturday : CLOSED</p>
                                        <p class="textGreen mb-0">Address</p>
                                        <p class="mb-0">Al Sadd , Suhaim Bin Hamad Street, Qatar First Bank Building ,</p>
                                        <p class="mt-0">8th floor P.O. Box: 11068, Doha, State of Qatar.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="textGreen mb-0">Phone Number</p>
                                        <p><a href="tel:974-44050555">974-44050555</a></p>
                                        <p class="textGreen mb-0">Fax</p>
                                        <p> 974-44050505 </p>
                                        <p class="textGreen mb-0">Email</p>
                                        <p> <a href="mailto:info@beema.com.qa">info@beema.com.qa</a></p>
                                    </div>
                                </div>
                                <div class="map margin_top"><iframe class="w-100" height="300" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3411.111071138104!2d29.965093864623746!3d31.24534841779793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssan+stefano+mall!5e0!3m2!1sen!2seg!4v1556046470452!5m2!1sen!2seg" allowfullscreen></iframe>                        </div>
                            </div>
                        </div>

                    </div>
                    <div class="tap animated fadeIn pt-3" id="selfServiceMachines">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a class="nav-link active" id="machine1" data-toggle="tab" href="#_machine1" role="tab" aria-controls="_machine1" aria-selected="true">Machine 1</a></li>
                            <li><a class="nav-link" id="machine2" data-toggle="tab" href="#_machine2" role="tab" aria-controls="_machine2" aria-selected="false">Machine 2</a></li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="_machine1" role="tabpanel" aria-labelledby="machine1">
                                <p>Location : Lorem ipsum</p>
                                <div class="map margin_top"><iframe class="w-100" height="300" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3411.111071138104!2d29.965093864623746!3d31.24534841779793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssan+stefano+mall!5e0!3m2!1sen!2seg!4v1556046470452!5m2!1sen!2seg" allowfullscreen></iframe></div>
                            </div>
                            <div class="tab-pane fade" id="_machine2" role="tabpanel" aria-labelledby="machine2">
                                <p>Location : Lorem ipsum</p>
                                <div class="map margin_top"><iframe class="w-100" height="300" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3411.111071138104!2d29.965093864623746!3d31.24534841779793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssan+stefano+mall!5e0!3m2!1sen!2seg!4v1556046470452!5m2!1sen!2seg" allowfullscreen></iframe></div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="textBlue text-center">To get a quote or for more details, please contact our team at 44050555.</h3>
            </div>
        </section>
    </main>



<?php get_footer(); ?>
