<?php
/* Template Name: News
*/
get_header(); ?>
<main class="site_main bg">
    <section class="topSection" style="background: url(<?php echo get_template_directory_uri();?>/assets/img/news.jpg);">
        <div class="container">
            <h1>News Listing</h1>
        </div>
    </section>
    <section class="news">
        <div class="container">
            <div class="news_items">
                <?php
                // WP_Query arguments
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $query = new WP_Query( array(
                    'post_type'      => 'post',
                    'posts_per_page' => 6,
                    'order'          => 'DESC',
                    'paged' =>$paged
                ) );

                // The Query

                // The Loop
                if ( $query->have_posts() ) {
                    ?>
                <nav aria-label="Page navigation" class="news-top-navigation">
                    <ul class="pagination justify-content-center mt-4">
                        <?php
                        echo paginate_links( array(
                            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                            'total'        => $query->max_num_pages,
                            'current'      => max( 1, get_query_var( 'paged' ) ),
                            'format'       => '?paged=%#%',
                            'show_all'     => false,
                            'type'         => 'plain',
                            'end_size'     => 2,
                            'mid_size'     => 1,
                            'prev_next'    => true,
                            'prev_text'    => sprintf( '<i></i> %1$s', __( 'Previous', 'text-domain' ) ),
                            'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
                            'add_args'     => false,
                            'add_fragment' => '',
                        ) );
                        ?>
                    </ul></nav>
                    <?php
                    while ( $query->have_posts() ) {
                        $query->the_post();
                       ?>
                        <div class="news_item d-flex">
                            <div class="img"><img src="<?php echo get_the_post_thumbnail_url();?>" alt="pic"/></div>
                            <div class="_content">
                                <p class="textGreen font-weight-bold"><?php the_title(); ?></p>
                                <p class="textGreen">2019/4/17</p>
                                <p><?php the_excerpt(); ?></p><a href="<?php the_permalink(); ?>" class="textGreen d-block mt-3">More <i class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                <?php
                    }
             ?>

            </div>
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center mt-4">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i></i> %1$s', __( 'Previous', 'text-domain' ) ),
                    'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
                </ul></nav>
            </div>
            <h3 class="textBlue mt-5 text-center">To get a quote or for more details, please contact our team at 44050555.</h3>
            <?php
            }

            // Restore original Post Data
            wp_reset_postdata();
            ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>
