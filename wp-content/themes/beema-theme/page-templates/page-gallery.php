<?php
/* Template Name: Gallery
*/
get_header();
?>
    <main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/gallery.jpg);">
            <div class="container">
                <?php if(is_rtl()){
                    ?>
                    <h1>معرض الصور</h1>
                    <?php
                }else{
                    ?>
                    <h1>Gallery</h1>
                <?php } ?>
            </div>
        </section>
        <section class="gallery">
            <div class="container">
                <div class="row">
                    <?php
                    // WP_Query arguments
                    $args = array(
                            'post_type'=>'gallery'
                    );

                    // The Query
                    $gallery = new WP_Query($args);

                    // The Loop
                    if ($gallery->have_posts()) {
                        while ($gallery->have_posts()) {
                            $gallery->the_post();
                          ?>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <div class="gallery_item">
                                    <p class="textGreen"><?php the_title(); ?></p><a href="<?php the_field('image'); ?>" target="_blank"><img class="w-100" src="<?php the_field('image'); ?>" alt="pic"/></a><a href="<?php the_field('image'); ?>" class="btn_style green_btn" download><?php if(is_rtl()){echo "تحميل";}else{echo "Download";} ?></a>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    // Restore original Post Data
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </section>
    </main>
<?php
get_footer();
