<?php
/* Template Name: General Assemply */
get_header(); ?>

  <main class="site_main">
  <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/investors/general-assembly/header.jpg);">
          <div class="container">
            <h1>General Assemply</h1>
          </div>
        </section>
        <section>
          <section class="quarterlyResults">
        <div class="container">
            <div class="accordion">
                <div class="card">
                    <div class="card-header">
                        <div class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" aria-expanded="true">Lorem Ipsum is simply dummy text of the printing and typesetting </button>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="card-body">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.


                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <div class="card">
                    <div class="card-header">
                        <div class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" aria-expanded="true">Lorem Ipsum is simply dummy text of the printing and typesetting </button>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="card-body">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.


                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <div class="card">
                    <div class="card-header">
                        <div class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" aria-expanded="true">Lorem Ipsum is simply dummy text of the printing and typesetting </button>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="card-body">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.


                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion">
                <div class="card">
                    <div class="card-header">
                        <div class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" aria-expanded="true">Lorem Ipsum is simply dummy text of the printing and typesetting </button>
                        </div>
                    </div>
                    <div class="collapse">
                        <div class="card-body">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
        </section>
      </main>

<?php get_footer(); ?>
