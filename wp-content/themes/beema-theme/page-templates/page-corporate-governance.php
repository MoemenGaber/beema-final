      <?php
/* Template Name: Corporate Governance */
get_header(); ?>

   <main class="site_main">
   <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/investors/corporate-governence/header.jpg);">
          <div class="container">
            <h1>Corporat Governance</h1>
          </div>
        </section>
        <section>
          <div class="container">
            <?php the_field('description'); ?>
            <h3 class="textBlue">Corporate Governance Reports</h3>
            <?php while(have_rows('report')){ the_row(); ?>
            <p><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a></p>
            <?php } ?>
          </div>
        </section>
      </main>

<?php get_footer(); ?>
