<?php
/* Template Name: Workmen’s Insurance */
get_header(); ?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/workmen.jpg);">
          <div class="container">
            <h1>Workmen’s Compensation Insurance</h1>
          </div>
        </section>
        <section class="insurance">
          <div class="container">
            <p class="textBlue font-weight-bold">WORKMEN’S COMPENSATION INSURANCE</p>
              <?php the_field('description'); ?>
            <h3 class="margin_top textBlue">To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>

<?php get_footer(); ?>
