<?php 
/* Template Name: Investors Relations */


get_header();
?>
   <main class="site_main investors-page">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/investors/investors-relations/header.jpg);">
          <div class="container">
            <h1>Investors Relations</h1>
          </div>
        </section>
        <section class="serviceOverview">
          <div class="container">
            <!-- <h2 class="textDarkBlue">Service Overview</h2> -->
            <!-- <p>Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level four loko PBR. Keytar pickled next level keffiyeh drinking vinegar street art. Art party vinyl Austin, retro whatever keytar mixtape. Pickled ethnic farm-to-table distillery ugh chia. Ethical Odd Future narwhal, mlkshk fap asymmetrical next level High Life literally cred blog. Banh mi swag art party, fashion axe you probably haven't heard of them stumptown tousled food truck post-ironic quinoa bicycle rights aesthetic keytar Pitchfork.</p> -->
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                <a class="investors-link" href="<?php echo get_permalink(get_page_by_path('quarterly-results')) ?>"><h3 class="mt-0 textBlue font-weight-bold">Quarterly Results</h3></a>
                  <p>
                   Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque deserunt nam illum quod id laudantium tempora ab tempore doloribus possimus. Veritatis magnam hic dolore temporibus ipsam id laborum rem nostrum.
                  </p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_path('quarterly-results')) ?>">Read more <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php echo get_template_directory_uri();?>/assets/img/investors/quarterly-results/small_image.jpg" alt="pic"/></div>
              </div>
              <div class="row margin_top_4">
              <div class="col-lg-6"><img class="w-100" src="<?php echo get_template_directory_uri();?>/assets/img/investors/annual-results/small_image.jpg" alt="pic"/></div>
                <div class="col-lg-6">
                <a class="investors-link" href="<?php echo get_permalink(get_page_by_path('annual-results')) ?>"><h3 class="mt-0 textBlue font-weight-bold">Annual Results</h3></a>
                  <p>
                   Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tenetur aut dolore atque distinctio quasi ab excepturi in corporis sit. Dolores veniam pariatur, minus hic et deserunt est quam obcaecati veritatis.
                  </p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_path('annual-results')) ?>">Read more <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                <a class="investors-link" href="<?php echo get_permalink(get_page_by_path('credit-rating')) ?>"><h3 class="mt-0 textBlue font-weight-bold">Credit Rating</h3></a>
                  <p>
                   Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque deserunt nam illum quod id laudantium tempora ab tempore doloribus possimus. Veritatis magnam hic dolore temporibus ipsam id laborum rem nostrum.
                  </p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_path('credit-rating')) ?>">Read more <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php echo get_template_directory_uri();?>/assets/img/investors/credit-rating/small_image.jpg" alt="pic"/></div>
              </div>
              <div class="row margin_top_4">
              <div class="col-lg-6"><img class="w-100" src="<?php echo get_template_directory_uri();?>/assets/img/investors/surplus-distruptions/small_image.jpg" alt="pic"/></div>
                <div class="col-lg-6">
                <a class="investors-link" href="<?php echo get_permalink(get_page_by_path('surplus-distribution')) ?>"> <h3 class="mt-0 textBlue font-weight-bold">Surplus Distribution</h3></a>
                  <p>
                   Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tenetur aut dolore atque distinctio quasi ab excepturi in corporis sit. Dolores veniam pariatur, minus hic et deserunt est quam obcaecati veritatis.
                  </p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_path('surplus-distribution')) ?>">Read more <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                <a class="investors-link" href="<?php echo get_permalink(get_page_by_path('general-assembly')) ?>"><h3 class="mt-0 textBlue font-weight-bold">General Assembly</h3></a>
                  <p>
                   Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque deserunt nam illum quod id laudantium tempora ab tempore doloribus possimus. Veritatis magnam hic dolore temporibus ipsam id laborum rem nostrum.
                  </p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_path('general_assembly')) ?>">Read more <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php echo get_template_directory_uri();?>/assets/img/investors/general-assembly/small_image.jpg" alt="pic"/></div>
              </div>
              <div class="row margin_top_4">
              <div class="col-lg-6"><img class="w-100" src="<?php echo get_template_directory_uri();?>/assets/img/investors/corporate-governence/small_image.jpg" alt="pic"/></div>
                <div class="col-lg-6">
                <a class="investors-link" href="<?php echo get_permalink(get_page_by_path('corporate-governance')) ?>"><h3 class="mt-0 textBlue font-weight-bold">Corporate Governance</h3></a>
                  <p>
                   Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tenetur aut dolore atque distinctio quasi ab excepturi in corporis sit. Dolores veniam pariatur, minus hic et deserunt est quam obcaecati veritatis.
                  </p><a class="green_btn btn_style" href="<?php echo get_permalink(get_page_by_path('corporate-governance')) ?>">Read more <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>

<?php
get_footer();