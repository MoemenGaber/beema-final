<?php 
/* Template Name: marine Insurance */


get_header();
?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/marine.jpg);">
          <div class="container">
            <h1>Marine Hull Insurance</h1>
          </div>
        </section>
        <section>
          <div class="container">
            <p><?php the_field('under_header_description'); ?></p>
            <p class="textBlue text-uppercase mt-4 mb-4">MARINE HULL INSURANCE</p>
            <p><?php the_field('marine_description'); ?></p>
            <p class="textBlue text-uppercase margin_top">MARINE CARGO INSURANCE</p>
            <p><?php the_field('cargo_description'); ?></p>
            <p class="textBlue text-uppercase">MARINE CARGO WAR AND STRIKES INSURANCE</p>
            <p><?php the_field('war_description'); ?></p>
            <p class="textBlue text-uppercase">NOTE: TYPES OF POLICIES:</p>
            <p>Beema issues   two   types of cargo insurance policies:</p>
            <img class="mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/marine-insurance.png" alt="pic"/>
            <p><?php the_field('note_description'); ?></p>
            <h3 class="textBlue mt-5">To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>

<?php
get_footer();