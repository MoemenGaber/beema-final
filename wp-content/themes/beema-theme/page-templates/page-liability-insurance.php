<?php 

/* Template Name: Liability Insurance */

get_header(); ?>

<main class="site_main bg">
        <section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/liability-insurance.jpg);">
          <div class="container">
            <h1>Liability Insurance</h1>
          </div>
        </section>
        <section class="insurance">
          <div class="container">
            <p class="text-Uppercase textBlue">GENERAL THIRD-PARTY LIABILITY INSURANCE</p>
            <p><?php the_field('general_description'); ?></p>
            <p class="text-Uppercase textBlue mt-4">PROFESSIONAL INDEMNITY INSURANCE</p>
            <p><?php the_field('professional_description'); ?></p>
            <p class="text-Uppercase textBlue mt-4">DESIGNERS, ARCHITECTS AND CONSULTANTS generally require  two   types of covers:</p>
            <?php the_field('designers_description'); ?>
            <p class="text-Uppercase textBlue mt-4">LIABILITY PRODUCTS OF INTEREST TO OTHER    PROFESSIONALS INCLUDE:</p>
            <?php the_field('liability_description'); ?>
            <p class="text-Uppercase textBlue mt-4">MOTOR TRADE INSURANCE</p>
            <p><?php the_field('motor_description'); ?></p>
            <p class="text-Uppercase textBlue mt-4">OTHER INSURANCE POLICIES</p>
              <?php the_field('other_description'); ?>
            <h3 class="textBlue mt-5">To get a quote or for more details, please contact our team at 44050555.</h3>
          </div>
        </section>
      </main>

<?php get_footer();