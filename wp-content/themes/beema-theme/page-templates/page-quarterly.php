<?php
/* Template Name: Quarterly results
*/
get_header();?>
<main class="site_main bg quarterly-results">
<section class="topSection" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/investors/quarterly-results/header.jpg);">
        <div class="container">
            <h1>Quarterly Results</h1>
        </div>
    </section>
    <section class="quarterlyResults">
        <div class="container">
            <?php  $id=1; ?>
                <?php while(have_rows('year_results')){ the_row(); ?>
            <div class="accordion">
            <div class="card">
                    <div class="card-header">
                        <div class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" aria-expanded="true"><?php the_sub_field('year'); ?></button>
                        </div>
                    </div>
                    <div class="collapse  <?php if($id==1){echo "show"; } ?>">
                        <div class="card-body">
                            <?php while(have_rows('result')){ the_row();

                            ?>
                            <div class="accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="mb-0">
                                           <a href="<?php echo the_sub_field('file'); ?>" download> <button class="btn btn-link" type="button" aria-expanded="true"><i class="fas fa-caret-right"></i><?php the_sub_field('title'); ?></button></a>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <?php


                            } ?>
                        </div>
                    </div>
                </div>
            </div>
                <?php
                    $id++;
                } ?>

        </div>
    </section>
</main>
<?php get_footer(); ?>
