<?php 
get_header(); 
?>

      <main class="site_main">
        <section class="topSection" style="background: url(..<?php echo get_template_directory_uri();?>/img/retail.jpg);">
          <div class="container">
            <h1><?php the_title(); ?></h1>
            <p><?php the_field('service_excerpt'); ?></p>
          </div>
        </section>
        <section class="serviceOverview">
          <div class="container">
            <h2 class="textDarkBlue"><?php $servies_overview=get_field('service_overview_title'); if($servies_overview){the_field('service_overview_title');}else{echo "Service Overview";} ?></h2>
            <p><?php the_field('service_overview_description'); ?></p>
            <div class="margin_top_4">
              <div class="row">
                <div class="col-lg-6"><img class="w-100" src="<?php the_field('first_image'); ?>" alt="pic"/></div>
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold"><?php the_field('first_paragraph_title'); ?></h3>
                  <p>
                   <?php the_field('first_paragraph'); ?>
                  </p><a class="green_btn btn_style" href="<?php the_field('first_paragraph_request_a_call_link'); ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
              </div>
              <div class="row margin_top_4 leftsec">
                <div class="col-lg-6">
                  <h3 class="mt-0 textBlue font-weight-bold"><?php the_field('second_paragraph_title'); ?></h3>
                  <p>
                    <?php the_field('second_paragraph'); ?>
                  </p><a class="green_btn btn_style" href="<?php the_field('first_paragraph_request_a_call_link'); ?>">Request a call <i class="fas fa-long-arrow-alt-right ml-2"></i></a>
                </div>
                <div class="col-lg-6"><img class="w-100" src="<?php the_field('second_image'); ?>" alt="pic"/></div>
              </div>
             
            </div>
          </div>
        </section>
        <section class="news pt-0">
          <div class="container">
            <h2 class="textDarkBlue">More Options for corp</h2>
            <p>Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level four loko PBR.</p>
            <div class="row">
              <?php 

// WP_Query arguments
$args = array(
  'post_type'              => array( 'corporate_services' ),
  'order'=>'ASC',
);

// The Query
$retal = new WP_Query( $args );

// The Loop
if ( $retal->have_posts() ) {
	while ( $retal->have_posts() ) {
		$retal->the_post();
    ?>
      <div class="col-md-4">
                <div class="blog_item mt-4"><a href="<?php the_permalink(); ?>"><img src="<?php the_field('first_image'); ?>" alt="pic"/>
                    <h3 class="font-weight-bold pr-2 pl-2"><?php the_title(); ?></h3>
                    <p class="pr-2 pl-2"><?php $length=100; $x=get_field('service_excerpt'); custom_limit($x,$length); ?></p><a class="textBlue pr-2 pl-2" href="<?php the_permalink(); ?>">Know More <i class="fas fa-long-arrow-alt-right ml-2"></i></a></a></div>
              </div>
    <?php
	}
} 

// Restore original Post Data
wp_reset_postdata();
              ?>
            </div>
          </div>
        </section>
      </main>
    </body>

<?php get_footer();